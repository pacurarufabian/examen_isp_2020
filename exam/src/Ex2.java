import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class GUIApp extends JFrame {
    JTextField field1, field2; //two text fields
    JButton show; //one button

    GUIApp() {
        setTitle("GUIApp"); //we set the title of the window
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //we set the default close operation
        init(); //we call method init
        setSize(350, 200); //we set dimensions for the window that will be created
        setVisible(true); //we set the window visible
    }

    public void init() {

        this.setLayout(null); //we set the layout of the window to null

        field1 = new JTextField(); //we declare the first text field
        field1.setBounds(20, 20, 300, 30); //we set dimensions and position of the text field within the window

        field2 = new JTextField(); //we declare the second text field
        field2.setBounds(20, 70, 300, 30); //we set dimensions and position of the text field within the window
        field2.setEditable(false); //we make the second text field not editable

        show = new JButton("Show"); //we declare the button
        show.setBounds(120, 120, 100, 30); //we set dimensions and position of the button within the window
        show.addActionListener(new Action()); //we add an action listener for the button in order to know when the button is pressed

        add(field1);
        add(field2);
        add(show); // we add all our elements to the window
    }

    public static void main(String[] args) {
        new GUIApp(); //we instantiate a new GUIApp
    }

    class Action implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            //this piece of code executes only if the button "show" is pressed
            field2.setText(field1.getText()); //we set the value of the second text field as the value of the first text field
        }
    }
}