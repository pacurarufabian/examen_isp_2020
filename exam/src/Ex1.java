class K {
}

class A {

    //aggregation between A and M
    private M m;

    A(M m) {
        this.m = m;
    }
    //

    public void metA() {
    }
}

class B {

    public void metB() {
    }
}

//inheritance between M and K
class M extends K {

    //Composition between M and B
    private B b;

    M() {
        this.b = new B();
    }
    //
}

class X {
}

class L {

    private int a;

    //directed association between L and M
    private M m;

    //dependency between L and X
    public void i(X x) {
    }
}